package TestsOnLeftHandTabsOnCRM;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.NotificationTests;

public class NotificationOnLeftHandMenu  extends NotificationTests{
	
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_Click_Notification() throws InterruptedException
	{
		Thread.sleep(1500);
		click_notification();
	}

	@Test
	public void b_Click_CreateNote() throws InterruptedException
	{

		Thread.sleep(500);
		click_createnote();
	}
	
	@Test
	public void e_Enter_Message() throws InterruptedException
	{

		Thread.sleep(500);
		enter_message();
	}
	
	@Test
	public void g_Click_Create() throws InterruptedException
	{

		Thread.sleep(1500); 
		click_create();	
	}
	
	@Test
	public void h_Check_Success_Message() throws InterruptedException
	{

		Thread.sleep(1500);
		Success_Msg();
	}
	
	@Test
	public void i_ClickEdit_notification() throws InterruptedException 
	{
		Thread.sleep(1000);
		edit_note();
		
	}
	
	@Test
	public void j_ChangeMessage() throws InterruptedException 
	{
		Thread.sleep(1000);
		change_Msg();
	}
	
	@Test
	public void k_clickApply_changes() throws InterruptedException 
	{
		Thread.sleep(1000);
		Click_applyChanges();
		
	}
	
	@Test
	public void l_checkEdit_SuccessMessage() throws InterruptedException 
	{
		Thread.sleep(1000);
		Edit_success_msg();
		
	}
	
	@Test
	public void la_VerifyCreatedNotes() throws InterruptedException 
	{
		Thread.sleep(1000);
		verify_note();
		
	}
	
	@Test
	public void m_Delete_notification() throws InterruptedException 
	{
		Thread.sleep(1200);
		deleteANDverify();
		
	}
	
	@Test
	public void n_Delete_note_Successmsg() throws InterruptedException 
	{
		Thread.sleep(1000);
		
		Success_Msg();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////
//*************************** Method to click notification tab ********************************
public void click_notification() throws InterruptedException
{  
	try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);

		WebElement notes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[8]/a[1]"));
		
		Boolean name= notes.isDisplayed();
		
		if(name==true) {
		    
			wait.equals(name);
		    
			Actions act= new Actions(driver);
			
			act.moveToElement(notes).click().build().perform();
			
			Thread.sleep(1000);
		}
	 	
	}catch(NoSuchElementException e) {
	
		WebElement notes= driver.findElement(By.linkText("Notification"));
	    
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).click().build().perform();
	}
}
	
//************************** Method to click create Notification **********************
public void click_createnote()
{
  try {
	
	  driver.findElement(By.id("B12131301940768901")).click();
		
  }catch(NoSuchElementException e) {
	
	  WebElement create=	driver.findElement(By.id("B12131301940768901"));
		
	  Actions act= new Actions(driver);
		
	  act.moveToElement(create).click().build().perform();
  }
}
		
//************************** Method to enter message **********************************
public void enter_message()
{
	WebElement mesg= driver.findElement(By.id("P65_CM_MESSAGE"));

	mesg.sendKeys("test message123");
			
}
		
//************************** Method to enter note ************************************
public void enter_note()
{
	WebElement note= driver.findElement(By.linkText("Notes"));

	note.sendKeys("test note 1234");
	
}
		
//************************** Method to click create *********************************
public void click_create()
{
	try {

		WebElement create= driver.findElement(By.id("B12132831571827550"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(create).click().build().perform();
		
	}catch(StaleElementReferenceException | NoSuchElementException e)	{
	
		WebElement create= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[2]/a[1]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(create).click().build().perform();
	}
}
		
//************************* Method to check success message *************************
public void Success_Msg()
{
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
			
	String Expected_msg= "Action Processed.";
			
	Assert.assertEquals(Msg, Expected_msg);
	
}

//************************* Method to verify a created notification *******************
public void verify_note()
{
	WebElement note= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));

	Boolean note1= note.isDisplayed();
	
	Assert.assertTrue(note1);
	
}
		
//************************* Method to edit a created notification *******************
public void edit_note()
{
	driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]")).click();

}

//************************ Method to change message of a notification ***************	
public void change_Msg()
{
	WebElement mesg= driver.findElement(By.id("P65_CM_MESSAGE"));

	mesg.clear();
	
	mesg.sendKeys("test message1235678");
	
}
		
//*********************** Method to click apply changes *****************************
public void Click_applyChanges()
{
	driver.findElement(By.linkText("Apply Changes")).click();

}

//*********************** Method to check edit success message **********************
public void Edit_success_msg()
{
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
			
	String Expected_msg= "Action Processed.";
			
	Assert.assertEquals(Msg, Expected_msg);
	
}

//********************** Method to delete notification *****************************
public void deleteNotification() throws InterruptedException
{
	try {
			edit_note();
			
			Thread.sleep(1000);
		   
			WebElement delete= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[2]/a[2]"));
		   
			Actions act= new Actions(driver);
		   
			act.moveToElement(delete).doubleClick().build().perform();

			Thread.sleep(1500);

			Alert alert= driver.switchTo().alert();
			
			alert.accept();
			
	}catch(UnhandledAlertException | NoAlertPresentException e)	{
				
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
	}
}

//************************* Method to verify a deleted notification *******************
public void deleteANDverify() throws InterruptedException
{
	String note= driver.findElement(By.xpath("//tr[2]//td[5]")).getText();

	System.out.println(note);
	
	deleteNotification();
	
	String note1= driver.findElement(By.xpath("//tr[2]//td[5]")).getText();
	
	System.out.println(note1);
	
	Assert.assertNotEquals(note, note1);
 }	
}
